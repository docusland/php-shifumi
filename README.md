# PHP-Shifumi



## Objectif pour l'apprenant
Mettre en oeuvre les capacités acquises en cours de formation en réinvestissant ce qui a été appris en situation professionelle en lien avec un besoin simple: réaliser une interface web permettant de jouer à Pierre Feuille Ciseaux contre un ordinateur.

## Présentation du projet
### Objet du document
Ce document s'adresse à des étudiants web en formation de développeur web.

### Objectifs du projet
Vous devez créer un site web permettant de jouer à shifumi. Pour ce faire, vous devez appliquer les bonnes méthodes de conception web.

Il est attendu, avant tout, de recevoir un livrable **fonctionnel** et de **qualité**. Ainsi privilégiez et peaufinez la première exercice et organisez-vous bien pour la répartition des tâches et la gestion du dépôt git.

### Critères d'acceptabilité du produit
- La logique implémentée doit utiliser le langage appris en cours : PHP 7.4 ou 8.1.
- Le(s) document(s) livrés doivent être responsive
- un projet complet et fonctionnel versionné sous git
- Le(s) document(s) livrés doivent être respectueux de la RGPD.
- l'application doit être accessible en français ou anglais au minimum
- Vous pouvez également héberger votre site sur le support de votre choix, voire sur [Heroku](https://www.heroku.com/)

## Règles du shifumi
Le shifumi est un jeu d'origine chinoise. L'objectif au sein de ce projet est de réaliser une application web où le visiteur pourra jouer contre l'ordinateur.

A chaque partie, le joueur choisit l'une des trois actions suivantes :

- la pierre
- les ciseaux
- la feuille

De façon générale, la pierre bat les ciseaux (en les émoussant), les ciseaux battent la feuille (en la coupant), la feuille bat la pierre (en l'enveloppant).

Ainsi chaque coup bat un autre coup, fait match nul contre son homologue et est battu par la troisième option.

# Exercices
## Etape 1

Concevez le site internet. Il doit être conçu en HTML 5, CSS 3 et PHP. Il doit permettre :

- à l'utilisateur de réaliser un choix.
- L'ordinateur génère aléatoirement sa réponse, puis un résultat est affiché en fonction des règles mentionnées ci-dessus.
- l'utilisateur peut ensuite cliquer sur un bouton pour rejouer.

Ce site doit être esthétique et intuitif pour l'utilisateur.
*
## Etape 2
Tant que le visiteur continue de jouer, les statistiques suivantes sont affichées à l'écran :

- Nombre de victoires du visiteur
- Nombre de victoire de l'ordinateur
- Heure où a commencé la première partie

## Etape 3
Le site internet implémente une intelligence artificielle de base nommée **HAL**. Il mémorise les parties précédentes et joue en conséquence :

- Tour 1 : **HAL** choisit une option aléatoirement.
- Tour 2 : en fonction du choix réalisé par l'utilisateur lors du tour 1, **HAL** choisit l'option permettant de battre cette option.
- Tour 3 : **HAL** répète ce qu'il a dit en tour 1
- Tour 4 : **HAL** donne une option qu'il n'a encore jamais dite ou qu'il n'a pas dite depuis longtemps.
- Tour 5 : **HAL** répète ce que le joueur a donné en tour 4
- sa logique boucle et retourne en tour 1

## Etape 4
En fin de partie, le site doit stocker au sein d'une base de données: le taux de réussite d'un visiteur, son adresse IP, le nombre de tours joués ainsi que le timestamp (date et heure de création).
Attention à stocker au sein de votre git, la structure SQL de la base de données.

# Conseils
- Vous pouvez utiliser des frameworks !

